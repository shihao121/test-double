package stub;


import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class GradeServiceTest {
    /* 需求描述：
    编写GradeService类的单元测试，单元测试calculateAverageGrades方法
    * */

    @Mock
    GradeSystem gradeSystem;

    @Test
    void shouldReturn90WhenCalculateStudentAverageGradeAndGradeIs80And90And100() {
        //assertThat(result, is(90.0));
        MockGradeSystem mockGradeSystem = new MockGradeSystem();
        GradeService service = new GradeService(mockGradeSystem);

        double averageGrades = service.calculateAverageGrades(1);

        assertEquals(90.0, averageGrades);
    }

    @Test
    void should_return_90() {

        initMocks(this);
        when(gradeSystem.gradesFor(1)).thenReturn(Arrays.asList(80.0, 90.0, 100.0));

        GradeService service = new GradeService(gradeSystem);
        assertEquals(90.0, service.calculateAverageGrades(1));

    }
}