package mock;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


class SecurityCenterTest {
    /* 需求描述：
    编写SecurityCenter类的单元测试，单元测试switchOn方法，不依赖于DoorPanel的close的方法实现
    * */

    @Mock
    DoorPanel doorPanel;

    private SecurityCenter securityCenter;

    @BeforeEach
    void setUp() {
        initMocks(this);
        securityCenter = new SecurityCenter(doorPanel);

    }

    @Test
    void shouldVerifyDoorIsClosed() {
        MockDoorPanel doorPanel = new MockDoorPanel();
        securityCenter = new SecurityCenter(doorPanel);
        securityCenter.switchOn();
        assertTrue(doorPanel.isCall());
    }

    @Test
    void shouldVerifyDoorIsClosedUseMock() {
        securityCenter.switchOn();
        verify(doorPanel).close();
    }
}
