package mock;

public class MockDoorPanel extends DoorPanel {

    private Boolean isCall = false;

    Boolean isCall() {
        return this.isCall;
    }

    @Override
    void close() {
        this.isCall = true;
    }
}
